# Przepiękny projekt grupy mistrzów
* Aby skopiować pliki `git clone ...`,
* Aby wysłać pliki stworzone od podstaw
    * `git add "plik"`
    * `git commit -m "Opis dodanych plików"`
    * `git push origin master`
* Aby zaproponować rozwiązanie którego nie jesteśmy pewni:
     * `git branch "nazwabrancha"`
     * `git checkout "nazwabrancha"`
     * `git commit -m "opis rozwiazania"`
   gdzie nazwabrancha jednym słowem streszcza zmiany, np : git branch 'rotatingmesh'
* Gdy jesteśmy pewni naszego rozwiązania z poprzedniego punktu
    * `git pull "nazwabrancha" master`
